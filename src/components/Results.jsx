import { useContext } from 'react';
import {
  TableContainer,
  Paper,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Typography,
} from '@mui/material';
import { green, red, grey } from '@mui/material/colors';
import { DataContext } from './QuestionsDataProvider';

export const getCorrectOption = (dict) => Object.keys(dict).find((key) => dict[key]) || null;

const Results = ({ questions, selectedLevels }) => {
  const total = questions.length;
  const correct = questions.filter((q) => q.answeredCorrectly).length;
  const { levels } = useContext(DataContext);

  const levelNames = () => {
    const label = selectedLevels === 1 ? 'Niveau' : 'Niveaus';
    const result = [];
    selectedLevels.map((sl) => result.push(levels[sl]));
    return `${label}: ${result.join(', ')}`;
  };

  return (
    <TableContainer component={Paper}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>
              <Typography sx={{ fontStyle: 'italic', color: grey[500] }}>
                vraag
              </Typography>
            </TableCell>
            <TableCell>
              <Typography sx={{ fontStyle: 'italic', color: grey[500] }}>
                antwoord
              </Typography>
            </TableCell>
            <TableCell>
              <Typography sx={{ fontStyle: 'italic', color: grey[500] }}>
                (moest zijn)
              </Typography>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {questions.map((q) => (
            <TableRow key={q.id}>
              <TableCell>
                <Typography>{q.text}</Typography>
              </TableCell>
              {q.answeredCorrectly && (
                <>
                  <TableCell>
                    <Typography sx={{ color: green[300] }}>
                      {q.answer}
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography>&nbsp;</Typography>
                  </TableCell>
                </>
              )}
              {!q.answeredCorrectly && (
                <>
                  <TableCell>
                    <Typography sx={{ color: red[300] }}>{q.answer}</Typography>
                  </TableCell>
                  <TableCell>
                    <Typography sx={{ color: green[300] }}>
                      {getCorrectOption(q.answerOptions)}
                    </Typography>
                  </TableCell>
                </>
              )}
            </TableRow>
          ))}
          <TableRow>
            <TableCell colSpan={3}>
              {correct === 0 && (
                <Typography variant="h5">Je hebt niets goed.</Typography>
              )}
              {correct !== 0 && (
                <Typography variant="h5">
                  Je hebt een {Math.round((correct / total) * 10)}
                </Typography>
              )}
            </TableCell>
          </TableRow>

          <TableRow>
            <TableCell colSpan={3}>{levelNames()}</TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default Results;
