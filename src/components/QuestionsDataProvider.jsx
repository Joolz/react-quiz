import { createContext, useState, useEffect, useMemo } from 'react';
import Papa from 'papaparse';

function shuffleDict(dict) {
  const keys = Object.keys(dict);
  keys.sort(() => Math.random() - 0.5);
  const shuffled = {};
  for (let i = 0; i < keys.length; i += 1) {
    shuffled[keys[i]] = dict[keys[i]];
  }
  return shuffled;
}
const shuffleArray = (originalArray) => {
  const array = [...originalArray];
  let currentIndex = array.length;
  let randomIndex;

  while (currentIndex !== 0) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;
    [array[currentIndex], array[randomIndex]] = [
      array[randomIndex],
      array[currentIndex],
    ];
  }

  return array;
};

function getRandomInt(min, max) {
  const ceilMin = Math.ceil(min);
  const floorMax = Math.floor(max);
  return Math.floor(Math.random() * (floorMax - ceilMin) + min);
}

const DataContext = createContext([]);

const QuestionsDataProvider = ({ children }) => {
  const [errors, setErrors] = useState(null);
  const [meta, setMeta] = useState(null);
  const [loading, setLoading] = useState(true);
  const [loadedQuestions, setLoadedQuestions] = useState(null);
  const [levels, setLevels] = useState(null);

  useEffect(() => {
    const debugTestTimeout = 100;
    setTimeout(() => {
      const fetchData = async () => {
        const file = './data/questions.csv';
        Papa.parse(file, {
          download: true,
          header: true,
          delimiter: ';',
          error: () => {
            setLoading(false);
          },
          complete: (res) => {
            setErrors(res.errors);
            setMeta(res.meta);

            const resData = res.data;
            const filtered = shuffleArray(resData);

            const questions = [];

            let questionId = 0;
            filtered.forEach((q) => {
              const answerOptions = {};
              answerOptions[q.correctAnswer] = true;

              while (Object.keys(answerOptions).length < 4) {
                const random = getRandomInt(0, resData.length);
                const candidate = resData[random].correctAnswer;
                if (!(candidate in Object.keys(answerOptions))) {
                  answerOptions[candidate] = false;
                }
              }

              questions.push({
                id: questionId,
                level: q.level,
                text: q.text,
                answerOptions: shuffleDict(answerOptions),
                answer: null,
                answered: false,
                answeredCorrectly: false,
              });
              questionId += 1;
            });

            setLoadedQuestions(questions);
            setLoading(false);
          },
        });
      };
      fetchData();

      const lvls = {};
      lvls[1] = '5e kyu';
      lvls[2] = '4e kyu';
      lvls[3] = '3e kyu';
      lvls[4] = '2e kyu';
      lvls[5] = '1e kyu';
      lvls[6] = '1e dan';
      lvls[9] = 'algemeen';

      setLevels(lvls);
    }, debugTestTimeout);
  }, []);

  const data = useMemo(
    () => ({
      loadedQuestions,
      levels,
      loading,
      errors,
      meta,
    }),
    [loadedQuestions, levels, loading, errors, meta],
  );

  return <DataContext.Provider value={data}>{children}</DataContext.Provider>;
};

export { QuestionsDataProvider, DataContext };
