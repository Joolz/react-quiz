import { useContext } from 'react';
import {
  FormGroup,
  FormControlLabel,
  Checkbox,
  Button,
  Typography,
} from '@mui/material';
import { DataContext } from './QuestionsDataProvider';

const Start = ({ onStart, onSelectedLevelsChange, goodToGo }) => {
  const { levels } = useContext(DataContext);

  const handleChange = (event) => {
    onSelectedLevelsChange(event);
  };
  const handleClick = () => {
    onStart();
  };

  return (
    <>
      <Typography>Train deze levels</Typography>
      <FormGroup>
        {Object.entries(levels).map(([k, v]) => (
          <FormControlLabel
            key={k}
            name={k}
            control={<Checkbox />}
            label={v}
            onChange={handleChange}
          />
        ))}
      </FormGroup>
      <Button disabled={!goodToGo} onClick={handleClick} variant="contained">
        Start de quiz
      </Button>
    </>
  );
};

export default Start;
