import { useState } from 'react';
import {
  FormControl,
  FormLabel,
  RadioGroup,
  Radio,
  FormControlLabel,
} from '@mui/material';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import HighlightOffOutlinedIcon from '@mui/icons-material/HighlightOffOutlined';
import { green, red } from '@mui/material/colors';

const Ask = ({ question, onAnswerChange }) => {
  const questionText = `./images/${question.text}.png`;
  const [imageVisible, setImageVisible] = useState(true);

  const handleChange = (event) => {
    setImageVisible(true);
    onAnswerChange(event.target.value);
  };

  const checkImage = () => {
    setImageVisible(false);
  };

  const correctAnswer = Object.keys(question.answerOptions).find(
    (key) => question.answerOptions[key] === true,
  );

  return (
    <>
      <FormControl>
        <FormLabel id="demo-radio-buttons-group-label">
          Wat betekent <b>{question.text}</b>?
        </FormLabel>
        <br />
        <RadioGroup
          aria-labelledby="demo-radio-buttons-group-label"
          name="radio-buttons-group"
          value={question.answer}
          onChange={handleChange}
        >
          {Object.entries(question.answerOptions).map(([key]) => (
            <FormControlLabel
              disabled={question.answered}
              label={key}
              key={key}
              value={key}
              control={<Radio />}
            />
          ))}
        </RadioGroup>
      </FormControl>
      <div>
        {question.answered && question.answeredCorrectly && (
          <>
            <CheckCircleOutlineIcon sx={{ color: green[300] }} />
            <br />
            {imageVisible ? (
              <img src={questionText} onError={checkImage} alt="techniek" />
            ) : null}
          </>
        )}
        {question.answered && !question.answeredCorrectly && (
          <>
            <HighlightOffOutlinedIcon sx={{ color: red[300] }} />
            <div>
              Juist antwoord: <b>{correctAnswer}</b>
            </div>
          </>
        )}
      </div>
    </>
  );
};

export default Ask;
