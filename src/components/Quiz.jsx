import { useState, useContext, useEffect } from 'react';
import Card from '@mui/material/Card';
import Box from '@mui/material/Box';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import { useTheme } from '@mui/material/styles';
import MobileStepper from '@mui/material/MobileStepper';
import Button from '@mui/material/Button';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import { Alert, CircularProgress } from '@mui/material';
import { DataContext } from './QuestionsDataProvider';
import Ask from './Ask';
import Results from './Results';
import Start from './Start';

const Quiz = () => {
  const theme = useTheme();
  const { loading, loadedQuestions, errors } = useContext(DataContext);
  const [questions, setQuestions] = useState([]);
  const [activeStep, setActiveStep] = useState();
  const [maxSteps, setMaxSteps] = useState(0);
  const [finished, setFinished] = useState(false);
  const [started, setStarted] = useState(false);
  const [selectedLevels, setSelectedLevels] = useState([]);

  useEffect(() => {
    if (!loading) {
      if (activeStep === undefined) {
        setActiveStep(0);
      }
    }
  }, [loading, activeStep]);

  const onLevelSelected = (event) => {
    const val = event.target.name;
    if (event.target.checked) {
      if (selectedLevels.indexOf(val) === -1) {
        setSelectedLevels([val, ...selectedLevels]);
      }
    } else if (selectedLevels.indexOf(val) !== -1) {
      setSelectedLevels(selectedLevels.filter((e) => e !== val));
    }
  };

  const handleNextStep = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBackStep = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  if (activeStep === undefined) {
    return (
      <>
        <CircularProgress />
        <Alert severity="info">Loading data....</Alert>
      </>
    );
  }

  if ((errors !== null && errors.length > 0) || questions === null) {
    return (
      <Alert severity="error">
        {errors !== null ? errors : 'no questions found'}
      </Alert>
    );
  }

  const onAnswerGiven = (val) => {
    questions[activeStep].answer = val;
    questions[activeStep].answered = true;
    questions[activeStep].answeredCorrectly = questions[activeStep].answerOptions[val];

    if (!questions[activeStep].answeredCorrectly) {
      console.log('------------------------------');
      console.log('User answer ', val);
      console.log('Options are ', questions[activeStep].answerOptions);
      console.log('Correct? ', questions[activeStep].answerOptions[val]);
    }

    setQuestions(
      questions.map((q) => {
        if (q.id === questions[activeStep].id) {
          return questions[activeStep];
        }
        return q;
      }),
    );
    setFinished(activeStep >= maxSteps - 1);
  };

  const onStarted = () => {
    setStarted(true);
    const filtered = loadedQuestions.filter((q) => Object.values(selectedLevels).includes(q.level));
    setQuestions(filtered);

    setMaxSteps(filtered === null ? 0 : filtered.length);
  };

  return (
    <Box sx={{ display: 'flex', mt: 5 }}>
      <Card sx={{ padding: '30px' }}>
        {!started && (
          <Start
            onStart={onStarted}
            onSelectedLevelsChange={onLevelSelected}
            goodToGo={selectedLevels && selectedLevels.length > 0}
          />
        )}
        {started && !finished && (
          <>
            <CardContent>
              <Ask
                question={questions[activeStep]}
                onAnswerChange={onAnswerGiven}
              />
            </CardContent>
            <CardActions>
              <MobileStepper
                variant="text"
                steps={maxSteps}
                position="static"
                activeStep={activeStep}
                nextButton={(
                  <Button
                    size="small"
                    onClick={handleNextStep}
                    disabled={
                      activeStep === maxSteps - 1
                      || !questions[activeStep].answered
                    }
                  >
                    Next
                    {theme.direction === 'rtl' ? (
                      <KeyboardArrowLeft />
                    ) : (
                      <KeyboardArrowRight />
                    )}
                  </Button>
                )}
                backButton={(
                  <Button
                    size="small"
                    onClick={handleBackStep}
                    disabled={activeStep === 0}
                  >
                    {theme.direction === 'rtl' ? (
                      <KeyboardArrowRight />
                    ) : (
                      <KeyboardArrowLeft />
                    )}
                    Back
                  </Button>
                )}
              />
            </CardActions>
          </>
        )}
        {started && finished && (
          <Results questions={questions} selectedLevels={selectedLevels} />
        )}
      </Card>
    </Box>
  );
};

export default Quiz;
