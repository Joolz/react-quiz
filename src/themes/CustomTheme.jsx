import { createTheme } from '@mui/material';

const CustomTheme = createTheme({
  drawer: {
    width: 300,
  },
  palette: {
    background: {
      default: '#f2f2f2',
    },
  },
});

export default CustomTheme;
