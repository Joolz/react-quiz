import { ThemeProvider } from '@mui/material';
import CssBaseline from '@mui/material/CssBaseline';
import Container from '@mui/material/Container';
import Quiz from './components/Quiz';
import CustomTheme from './themes/CustomTheme';
import { QuestionsDataProvider } from './components/QuestionsDataProvider';

function App() {
  return (
    <div className="App">
      <ThemeProvider theme={CustomTheme}>
        <CssBaseline />
        <Container maxWidth="sm">
          <QuestionsDataProvider>
            <Quiz />
          </QuestionsDataProvider>
        </Container>
      </ThemeProvider>
    </div>
  );
}

export default App;
