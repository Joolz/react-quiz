# React quiz

Test project with questions about judo terms. To use the code:

- clone this repo
- npm i
- run one of the commands in `package.json`, e.g. npm run dev

Work on the new questions (post 1.0.0) comes from [https://judo.ijf.org/classification](https://judo.ijf.org/classification)